# Babysitting Fee Calculator #

This project is an implementation of the Babysitter Code Kata, by Kat (Rae) Tillmann.

## Getting Started ##

NOTE: I am new to gradle. I've tested this process on a couple of Windows 10 machines, with Java JDK 1.8.0_161 pre-installed. Feel free to let me know if something doesn't work as expected.

1. $ git clone https://KatRaeTillmann@bitbucket.org/KatRaeTillmann/babysitting-fee-calculator.git
2. $ cd babysitting-fee-calculator
3. $ gradelw build

To run the application: $ gradlew run

To run the tests: $ gradlew test

*You may need 'gradlew.bat' instead of 'gradlew'.*

Alternatively, you should be able to import and open this project in intellij. Also, there is a .zip in downloads. Just in case.

Please don't hesitate to reach out with questions or comments, or just to say 'Hi!'

kathleentillmann@yahoo.com
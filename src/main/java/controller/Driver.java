package controller;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.GUI;

public class Driver extends Application{

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Babysitting Fee Calculator");
        GUI gui = new GUI();
        Scene scene = new Scene(gui, 350, 300);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

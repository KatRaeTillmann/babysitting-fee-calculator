package view;

import controller.GUIController;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class GUI extends VBox {
    private ChoiceBox<String> startTimeSelector;
    private ChoiceBox<String> bedTimeSelector;
    private ChoiceBox<String> endTimeSelector;
    private TextField displayFeeTextField;
    private GUIController controller;

    public GUI() {
        this.setSpacing(20);
        this.setAlignment(Pos.CENTER);
        this.buildView();
    }

    private void buildView() {
        this.constructStartTimeBox();
        this.constructBedTimeBox();
        this.constructEndTimeBox();
        this.constructCalculateBox();
    }

    private void constructStartTimeBox() {
        HBox startTimeContainer = this.getContainer();
        Label startTimeLabel = this.getLabel("Start Time");
        startTimeSelector = this.getChoiceBox();
        startTimeContainer.getChildren().addAll(startTimeLabel, startTimeSelector);
        this.getChildren().add(startTimeContainer);
    }

    private void constructBedTimeBox() {
        HBox bedTimeContainer = this.getContainer();
        Label bedTimeLabel = this.getLabel("Bed Time");
        bedTimeSelector = this.getChoiceBox();
        bedTimeContainer.getChildren().addAll(bedTimeLabel, bedTimeSelector);
        this.getChildren().add(bedTimeContainer);
    }

    private void constructEndTimeBox() {
        HBox endTimeContainer = this.getContainer();
        Label endTimeLabel = this.getLabel("End Time");
        endTimeSelector = this.getChoiceBox();
        endTimeContainer.getChildren().addAll(endTimeLabel, endTimeSelector);
        this.getChildren().add(endTimeContainer);
    }

    private void constructCalculateBox() {
        HBox calculateContainer = this.getContainer();
        this.displayFeeTextField = this.getTextField();
        Button calculateButton = this.getCalculateButton();
        calculateContainer.getChildren().addAll(displayFeeTextField, calculateButton);
        this.getChildren().add(calculateContainer);
    }

    private HBox getContainer() {
        HBox container = new HBox(15);
        container.setAlignment(Pos.CENTER);
        return container;
    }

    private Label getLabel(String text) {
        Label label = new Label(text);
        label.setMinWidth(60);
        label.setAlignment(Pos.CENTER_RIGHT);
        return label;
    }

    private ChoiceBox<String> getChoiceBox() {
        ChoiceBox<String> box = new ChoiceBox<>();
        box.setMinWidth(75);
        box.getItems().addAll("5 pm", "6 pm", "7 pm", "8 pm",
                "9 pm", "10 pm", "11 pm", "12 am", "1 am", "2 am", "3 am", "4 am");
        box.setValue("5 pm");
        return box;
    }

    private TextField getTextField() {
        TextField field = new TextField();
        field.setPrefWidth(60);
        field.setDisable(true);
        field.setStyle("-fx-opacity: 1.0;");
        field.setAlignment(Pos.CENTER_RIGHT);
        return field;
    }

    private Button getCalculateButton() {
        Button calculateButton = new Button("Calculate");
        calculateButton.setMinWidth(75);
        calculateButton.setOnAction(event -> this.displayFee());
        return calculateButton;
    }

    private void displayFee() {
        try {
            this.getController(startTimeSelector, bedTimeSelector, endTimeSelector);
            int fee = this.controller.getFee();
            this.displayFeeTextField.setText("$" + fee + ".00");
        } catch (Exception ex) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Calculation Error");
            alert.setHeaderText("Please check selected times.");
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }
    }

    private void getController(ChoiceBox<String> startTimeSelector, ChoiceBox<String> bedTimeSelector,
                               ChoiceBox<String> endTimeSelector) {
        int startTimeAsInt = this.getChoiceValueAsInt(startTimeSelector.getValue());
        int bedTimeAsInt = this.getChoiceValueAsInt(bedTimeSelector.getValue());
        int endTimeAsInt = this.getChoiceValueAsInt(endTimeSelector.getValue());
        try {
            this.controller = new GUIController(startTimeAsInt, bedTimeAsInt, endTimeAsInt);
        } catch (Exception ex) {
            throw ex;
        }
    }

    private int getChoiceValueAsInt(String choiceValue) {
        int spaceIndex = choiceValue.indexOf(" ");
        return Integer.parseInt(choiceValue.substring(0, spaceIndex));
    }
}

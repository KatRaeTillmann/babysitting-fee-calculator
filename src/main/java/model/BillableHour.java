package model;

public class BillableHour {
    private Hour hour;

    /**
     * Constructor creates a BillableHour object and sets its hour by using a specified input time to determine
     * a corresponding hour from within a limited range of possible babysitting hours.
     *
     * @param inputTime Specified input time.
     * @throws IllegalArgumentException When specified input time correlates to an hour that is out of range.
     */
    public BillableHour(int inputTime) throws IllegalArgumentException {
        if (inputTime == 5) {
            this.hour = Hour.ZERO;
        } else if (inputTime == 6) {
            this.hour = Hour.ONE;
        } else if (inputTime == 7) {
            this.hour = Hour.TWO;
        } else if (inputTime == 8) {
            this.hour = Hour.THREE;
        } else if (inputTime == 9) {
            this.hour = Hour.FOUR;
        } else if (inputTime == 10) {
            this.hour = Hour.FIVE;
        } else if (inputTime == 11) {
            this.hour = Hour.SIX;
        } else if (inputTime == 12) {
            this.hour = Hour.SEVEN;
        } else if (inputTime == 1) {
            this.hour = Hour.EIGHT;
        } else if (inputTime == 2) {
            this.hour = Hour.NINE;
        } else if (inputTime == 3) {
            this.hour = Hour.TEN;
        } else if (inputTime == 4) {
            this.hour = Hour.ELEVEN;
        } else {
            throw new IllegalArgumentException("Invalid input time.");
        }
    }

    public int getHour() {
        return this.hour.value;
    }

    private enum Hour {
        ZERO(0), ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), ELEVEN(11);
        private int value;

        Hour(int value) {
            this.value = value;
        }
    }
}

package model;

public class PremiumRateHoursSubCalculator {
    private static final BillableHour MIDNIGHT = new BillableHour(12);
    private int numberOfPremiumRateHours;

    /**
     * Constructor calculates the number of BillableHours at the premium rate
     *
     * @param start BillableHour associated with start of babysitting shift
     * @param bed BillableHour associated with bed time
     * @param end BillableHour associated with end of babysitting shift
     */
    public PremiumRateHoursSubCalculator(BillableHour start, BillableHour bed, BillableHour end) throws IllegalArgumentException {
        if (bed.getHour() > end.getHour()) {
            throw new IllegalArgumentException("Bed time cannot be later than end time.");
        }
        if (start.getHour() > this.MIDNIGHT.getHour()) {
            this.setNumberOfPremiumRateHours(start.getHour(), end.getHour());
        } else if (end.getHour() > this.MIDNIGHT.getHour()) {
            this.setNumberOfPremiumRateHours(this.MIDNIGHT.getHour(), end.getHour());
        } else {
            this.numberOfPremiumRateHours = 0;
        }
    }

    private void setNumberOfPremiumRateHours(int midnight, int end) {
        this.numberOfPremiumRateHours = end - midnight;
    }

    public int getNumberOfPremiumRateHours() {
        return this.numberOfPremiumRateHours;
    }
}

package model;

public class BillableHoursCalculator {
    private int numberOfRegularRateHours;
    private int numberOfPremiumRateHours;
    private int numberOfReducedRateHours;

    /**
     * Constructor calculates the number of BillableHours for each of the following:
     * regular rate, premium rate, reduced rate
     *
     * @param start BillableHour associated with start of babysitting shift
     * @param bed BillableHour associated with bed time
     * @param end BillableHour associated with end of babysitting shift
     */
    public BillableHoursCalculator(BillableHour start, BillableHour bed, BillableHour end) {
        this.numberOfRegularRateHours = new RegularRateHoursSubCalculator(start, bed).getNumberOfRegularRateHours();
        this.numberOfPremiumRateHours = new PremiumRateHoursSubCalculator(start, bed, end).getNumberOfPremiumRateHours();
        this.setNumberOfReducedRateHours(start, end);
    }

    private void setNumberOfReducedRateHours(BillableHour start, BillableHour end) {
        int totalNumberOfHours = end.getHour() - start.getHour();
        this.numberOfReducedRateHours = totalNumberOfHours - numberOfPremiumRateHours - numberOfRegularRateHours;
    }

    public int getNumberOfRegularRateHours() {
        return this.numberOfRegularRateHours;
    }

    public int getNumberOfPremiumRateHours() {
        return this.numberOfPremiumRateHours;
    }

    public int getNumberOfReducedRateHours() {
        return this.numberOfReducedRateHours;
    }
}

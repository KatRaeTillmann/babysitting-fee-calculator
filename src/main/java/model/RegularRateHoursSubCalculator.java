package model;

public class RegularRateHoursSubCalculator {
    private static final BillableHour MIDNIGHT = new BillableHour(12);
    private int numberOfRegularRateHours;

    /**
     * Constructor calculates the number of BillableHours at the regular rate
     *
     * @param start BillableHour associated with start of babysitting shift
     * @param bed BillableHour associated with bed time
     */
    public RegularRateHoursSubCalculator(BillableHour start, BillableHour bed) throws IllegalArgumentException {
        if (start.getHour() > bed.getHour()) {
            throw new IllegalArgumentException("Start time cannot be later than bed time.");
        }
        if (start.getHour() > MIDNIGHT.getHour()) {
            this.numberOfRegularRateHours = 0;
        } else if (bed.getHour() > MIDNIGHT.getHour()) {
            this.setNumberOfRegularRateHours(start.getHour(), MIDNIGHT.getHour());
        } else {
            this.setNumberOfRegularRateHours(start.getHour(), bed.getHour());
        }
    }

    private void setNumberOfRegularRateHours(int start, int bed) {
        this.numberOfRegularRateHours = bed - start;
    }

    public int getNumberOfRegularRateHours() {
        return this.numberOfRegularRateHours;
    }
}

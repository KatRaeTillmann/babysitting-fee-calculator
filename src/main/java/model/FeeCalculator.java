package model;

public class FeeCalculator {
    private static final int REGULAR_RATE = 12;
    private static final int PREMIUM_RATE = 16;
    private static final int REDUCED_RATE = 8;
    private int fee;

    /**
     * Constructor calculates total fee based on specified times and pre-determined rates
     *
     * @param startTime Start time of babysitting shift
     * @param bedTime Bed time
     * @param endTime End time of babysitting shift
     */
    public FeeCalculator(int startTime, int bedTime, int endTime) throws IllegalArgumentException {
        if (startTime < 1 || bedTime < 1 || endTime < 1) {
            throw new IllegalArgumentException("Input times must be positive numbers.");
        }
        if (startTime > 12 || bedTime > 12 || endTime > 12) {
            throw new IllegalArgumentException("Input times must each be less than thirteen.");
        }
        this.setFee(startTime, bedTime, endTime);
    }

    private void setFee(int startTime, int bedTime, int endTime) {
        BillableHoursCalculator hoursCalculator = new BillableHoursCalculator(
                new BillableHour(startTime),
                new BillableHour(bedTime),
                new BillableHour(endTime));
        int feeForRegularRateBillableHours = hoursCalculator.getNumberOfRegularRateHours() * FeeCalculator.REGULAR_RATE;
        int feeForPremiumRateBillableHours = hoursCalculator.getNumberOfPremiumRateHours() * FeeCalculator.PREMIUM_RATE;
        int feeForReducedRateBillableHours = hoursCalculator.getNumberOfReducedRateHours() * FeeCalculator.REDUCED_RATE;
        this.fee = feeForRegularRateBillableHours + feeForPremiumRateBillableHours + feeForReducedRateBillableHours;
    }

    public int getFee() {
        return this.fee;
    }
}

package model;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

public class BillableHoursCalculatorTest {
    // For a full list of hours and associated input times,
    // please see BillableHour.
    BillableHour Zero = new BillableHour(5);
    BillableHour One = new BillableHour(6);
    BillableHour Two = new BillableHour(7);
    BillableHour Eight = new BillableHour(1);
    BillableHour Nine = new BillableHour(2);
    BillableHour Ten = new BillableHour(3);

    @Test(expected = IllegalArgumentException.class)
    public void whenBillableHoursCalculatorIsPassedStartHourGreaterThanBedHourItThrowsIllegalArgumentException() {
        new BillableHoursCalculator(One, Zero, Zero);
    }

    @Test
    public void whenBillableHoursCalculatorIsPassedValidHoursItReturnsExpectedNumberOfRegularRateHours() {
        assertEquals(0, new BillableHoursCalculator(Eight, Nine, Nine).getNumberOfRegularRateHours());
        assertEquals(1, new BillableHoursCalculator(Zero, One, One).getNumberOfRegularRateHours());
    }

    @Test
    public void whenBillableHoursCalculatorIsPassedBedHourGreaterThanMidnightItSubstitutesMidnightForBedHourInRegularRateHoursCalculation() {
        assertEquals(7, new BillableHoursCalculator(Zero, Eight, Eight).getNumberOfRegularRateHours());
        assertEquals(7, new BillableHoursCalculator(Zero, Nine, Nine).getNumberOfRegularRateHours());
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenBillableHoursCalculatorIsPassedBedHourGreaterThanEndHourItThrowsIllegalArgumentException() {
        new BillableHoursCalculator(Zero, Two, One);
    }

    @Test
    public void whenBillableHoursCalculatorIsPassedValidHoursItReturnsExpectedNumberOfPremiumRateHours() {
        assertEquals(0, new BillableHoursCalculator(Zero, One, One).getNumberOfPremiumRateHours());
        assertEquals(1, new BillableHoursCalculator(Zero, Zero, Eight).getNumberOfPremiumRateHours());
    }

    @Test
    public void whenBillableHoursCalculatorIsPassedStartHourGreaterThanMidnightItSubstitutesStartHourForMidnightInPremiumRateHoursCalculation() {
        assertEquals(1, new BillableHoursCalculator(Eight, Nine, Nine).getNumberOfPremiumRateHours());
        assertEquals(2, new BillableHoursCalculator(Eight, Ten, Ten).getNumberOfPremiumRateHours());
    }

    @Test
    public void whenBillableHoursCalculatorIsPassedValidHoursItReturnsExpectedNumberOfReducedRateHours() {
        assertEquals(0, new BillableHoursCalculator(Zero, One, One).getNumberOfReducedRateHours());
        assertEquals(1, new BillableHoursCalculator(Zero, One, Two).getNumberOfReducedRateHours());
        assertEquals(0, new BillableHoursCalculator(Eight, Nine, Ten).getNumberOfReducedRateHours());
    }
}

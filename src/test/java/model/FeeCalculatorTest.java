package model;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

public class FeeCalculatorTest {
    @Test(expected = IllegalArgumentException.class)
    public void whenFeeCalculatorIsPassedATimeThatIsTooSmallItThrowsIllegalArgumentException() {
        new FeeCalculator(0, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenFeeCalculatorIsPassedATimeThatIsTooLargeItThrowsIllegalArgumentException() {
        new FeeCalculator(1, 1, 13);
    }

    @Test
    public void whenFeeCalculatorIsPassedValidValuesItReturnsExpectedFee() {
        assertEquals(0, new FeeCalculator(5, 5, 5).getFee());
        assertEquals(92, new FeeCalculator(5, 6, 2).getFee());
    }
}

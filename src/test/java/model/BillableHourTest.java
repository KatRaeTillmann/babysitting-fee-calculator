package model;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

public class BillableHourTest {

    @Test(expected = IllegalArgumentException.class)
    public void whenBillableHourIsPassedTooSmallValueItThrowsIllegalArgumentException(){
        new BillableHour(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenBillableHourIsPassedTooLargeValueItThrowsIllegalArgumentException(){
        new BillableHour(13);
    }

    @Test
    public void whenBillableHourIsPassedAValidInputTimeItReturnsCorrespondingHour(){
        BillableHour testBillableHour;
        int[] possibleInputTimes = {5, 6, 7, 8, 9, 10, 11, 12, 1, 2,  3,  4};
        int correspondingHour = 0;

        for (int inputTime : possibleInputTimes) {
            testBillableHour = new BillableHour(inputTime);
            assertEquals(correspondingHour, testBillableHour.getHour());
            correspondingHour++;
        }
    }
}
